//  author Piers Rueb

	//  globals

	var htmlCont,
		cssCont,
		infoOn = false,
		scrollingState = false,
		myStyle = document.getElementsByTagName('style')[0];
		thisId = function(id){ return document.getElementById(id); };

	thisId('html').value = '<p>Take me out to the ballgame.</p>';
	thisId('css').value = 'p{\n    font-size: 13px;\n    }';

	function focusOnLoad(){
		thisId('html').focus();
	}
	focusOnLoad();

	//  html

	thisId('html').addEventListener('change', htmlChange);

	function htmlChange(){
		htmlCont = thisId('html').value;
		thisId('output').innerHTML = htmlCont;
	}

	//  css

	thisId('css').addEventListener('change', cssChange);

	function cssChange(){
		cssCont = thisId('css').value;
		myStyle.innerHTML = cssCont;
	}

	function areaCheck(){
		cssChange();
		htmlChange();
	}
	areaCheck();

	thisId('html').addEventListener('keypress', chTest);
	thisId('css').addEventListener('keypress', chTest);

	function chTest(){
		areaCheck();
		console.log('* Looking for changes within the editor *');  //  check for characters after the keypress event
		setTimeout(function(){
			areaCheck();
			console.log('* Double checking to be sure *');
		}, 1000); //  check for additional characters after the keypress event
	}

	thisId('scroll-button').addEventListener('click', scrollTest);

	function scrollTest(){
		if(scrollingState == false){
			thisId('toggle').setAttribute('class', 'scroll-on');
			thisId('output').setAttribute('class', 'scrollable');
			scrollingState = true;
		} else{
			thisId('toggle').setAttribute('class', '');
			thisId('output').setAttribute('class', '');
			scrollingState = false;
		}
		return false;
	}

	thisId('scroll-button').addEventListener('mouseover', showInfo);
	thisId('scroll-button').addEventListener('mouseout', showInfo);

	function showInfo(){
		if(infoOn == false){
			thisId('scroll-info').setAttribute('class', 'fadein');
			infoOn = true;
		} else{
			thisId('scroll-info').setAttribute('class', '');
			infoOn = false;
		}
	}

	//  scrollbar test

	// function measureScrollBar(){
	//
	// 	//  calculate scroll bar width
	// 	var outBar = $('#test-wrapper').width();
	// 	var inBar = $('#test-inner').width();
	// 	var scrollBarWidth = outBar - inBar;
	//
	// 	if(scrollBarWidth > 0){
	// 		//  do nothing
	// 	} else{
	// 		$('a.scroll-button, .scroll-info').css({'display' : 'none'});
	// 		$('section#output').css({'overflow-y' : 'scroll'});
	// 	}
	//
	// }
	// measureScrollBar();
